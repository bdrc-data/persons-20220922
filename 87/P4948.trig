@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:P4948 {
    bda:LG0P4948_DG8BO9CVD8VH
        a                   adm:UpdateData ;
        adm:logDate         "2024-01-17T15:42:29.325564Z"^^xsd:dateTime ;
        adm:logMessage      "adding names based on a note by J. Silk"@en ;
        adm:logWho          bdu:U1653882847 .
    
    bda:LG0P4948_IS5MUP8NYNCI
        a                   adm:UpdateData ;
        adm:logDate         "2024-01-17T15:43:33.652631Z"^^xsd:dateTime ;
        adm:logMessage      "fix sskt"@en ;
        adm:logWho          bdu:U1653882847 .
    
    bda:LG3BD411E98F8B56D0
        a                   adm:UpdateData ;
        adm:logDate         "2012-09-19T14:25:15.566Z"^^xsd:dateTime ;
        adm:logMessage      "yig nor bcos"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGD5556FC5161DC902
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:P4948  a            adm:AdminData ;
        adm:adminAbout      bdr:P4948 ;
        adm:facetIndex      13 ;
        adm:gitPath         "87/P4948.trig" ;
        adm:gitRepo         bda:GR0006 ;
        adm:graphId         bdg:P4948 ;
        adm:logEntry        bda:LG0P4948_DG8BO9CVD8VH , bda:LG0P4948_IS5MUP8NYNCI , bda:LG3BD411E98F8B56D0 , bda:LGD5556FC5161DC902 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVC50E3A5AF2B012BF
        a                   bdo:PersonBirth ;
        bdo:eventWhen       "1820"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWhere      bdr:G1359 .
    
    bdr:EVD090793A340A2E4B
        a                   bdo:PersonDeath ;
        bdo:eventWhen       "1882"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:EVF6FC2A3040832142
        a                   bdo:PersonOccupiesSeat ;
        bdo:eventWhere      bdr:G498 .
    
    bdr:NM1BCC9E7DB45DD9C4
        a                   bdo:PersonPersonalName ;
        rdfs:label          "artha sid d+ha ba dz+ra/"@bo-x-ewts .
    
    bdr:NM2040D0E609CDEB03
        a                   bdo:PersonPrimaryTitle ;
        rdfs:label          "yil go san hu thog thu/"@bo-x-ewts .
    
    bdr:NM36C2D9173E7B8E48
        a                   bdo:PersonPersonalName ;
        rdfs:label          "yil go san hu thog thu blo bzang bsam grub/"@bo-x-ewts .
    
    bdr:NMB3ABF03D697A5372
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "blo bzang bsam grub/"@bo-x-ewts .
    
    bdr:NMP49483TOHNHFI
        a                   bdo:PersonTulkuTitle ;
        rdfs:label          "mchog sprul rin po che/"@bo-x-ewts .
    
    bdr:NMP4948JBLBH6RX
        a                   bdo:PersonPersonalName ;
        rdfs:label          "Arthasiddhavajra"@sa-x-iast .
    
    bdr:NMP4948LGWSUX8N
        a                   bdo:PersonPrimaryTitle ;
        rdfs:label          "sngags rams pa chos rje lcam sring skyabs/"@bo-x-ewts .
    
    bdr:NT0E564F1445F58BB7
        a                   bdo:Note ;
        bdo:noteText        "Old TSD Schools table tree: \n\n-- Bka'-gdams-pa\n\n-- Dga'-ldan-pa"@en .
    
    bdr:NT19566B9BB774AF77
        a                   bdo:Note ;
        bdo:noteText        "There exists a two-part biography of this incarnation:\nYil go san hu thog thu blo bzang bsam grub kyi rnam thar nor bu'i 'phreng ba ff. 38\nDe'i kha skong pad dkar phreng ba  ff. 11\nFound in vol. ka of the 4 vol., largely printed, gsung 'bum described in MHTL.\nMHTL 6625-6626.\nMHTL, v. 2, p. 45 describes a five vol. gsung 'bum, 3 printed volumes and 2 manuscript volumes in the National Library in Ulan Bator.\nCompiled a collection of rituals on Sprin ring gi rgyal po = Kuang ti = Ge-sar, 2 vols. of which 63 works are by himself."@en .
    
    bdr:NT64098982EA16F615
        a                   bdo:Note ;
        bdo:contentLocationStatement  "Vol. 2, p. 54" ;
        bdo:noteSource      bdr:MW19802 .
    
    bdr:P4948  a            bdo:Person ;
        skos:prefLabel      "yil go san hu thog thu/"@bo-x-ewts , "Ācārya Siddhavajra"@sa-x-iast ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:note            bdr:NT0E564F1445F58BB7 , bdr:NT19566B9BB774AF77 , bdr:NT64098982EA16F615 ;
        bdo:personEvent     bdr:EVC50E3A5AF2B012BF , bdr:EVD090793A340A2E4B , bdr:EVF6FC2A3040832142 ;
        bdo:personGender    bdr:GenderMale ;
        bdo:personName      bdr:NM1BCC9E7DB45DD9C4 , bdr:NM2040D0E609CDEB03 , bdr:NM36C2D9173E7B8E48 , bdr:NMB3ABF03D697A5372 , bdr:NMP49483TOHNHFI , bdr:NMP4948JBLBH6RX , bdr:NMP4948LGWSUX8N ;
        bdo:personStudentOf  bdr:P5325 .
}
