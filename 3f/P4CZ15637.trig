@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix edtf:  <http://id.loc.gov/datatypes/edtf/> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:P4CZ15637 {
    bda:LG0BDSVQWLDY5FLJ7S
        a                   adm:UpdateData ;
        adm:logAgent        "IATTC/scripts/write_rdf.py" ;
        adm:logDate         "2024-11-08T09:17:19.535960"^^xsd:dateTime ;
        adm:logMessage      "import data from the ATII project"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGAC78A26652A4F8E0
        a                   adm:InitialDataCreation ;
        adm:logDate         "2012-10-26T16:21:46.217000+00:00"^^xsd:dateTime ;
        adm:logMessage      "new and added info"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:P4CZ15637  a        adm:AdminData ;
        adm:adminAbout      bdr:P4CZ15637 ;
        adm:facetIndex      9 ;
        adm:gitPath         "3f/P4CZ15637.trig" ;
        adm:gitRepo         bda:GR0006 ;
        adm:graphId         bdg:P4CZ15637 ;
        adm:logEntry        bda:LG0BDSVQWLDY5FLJ7S , bda:LGAC78A26652A4F8E0 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV95214FD912B953B5
        a                   bdo:PersonEventNotSpecified .
    
    bdr:EVP4CZ15637_ATII_0
        a                   bdo:PersonFlourished ;
        bdo:eventWhen       "0500/0550"^^edtf:EDTF .
    
    bdr:NM7A553D2378F8CECB
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "rnam grol sde/"@bo-x-ewts .
    
    bdr:NM9FD1E5359B9F22C4
        a                   bdo:PersonOtherName ;
        rdfs:label          "shAkya'i dge slong 'phags pa rnam par grol ba'i sde/"@bo-x-ewts .
    
    bdr:NMB733071793762DB7
        a                   bdo:PersonPrimaryTitle ;
        rdfs:label          "'phags pa rnam grol sde/"@bo-x-ewts .
    
    bdr:NMC34FB8F4B772C1A4
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "解脱军｜圣解脱军"@zh-hans .
    
    bdr:NMP4CZ15637_ATII_SA0
        a                   bdo:PersonOtherName ;
        rdfs:label          "**Vimuktasena"@sa-x-iast .
    
    bdr:NMP4CZ15637_ATII_SA1
        a                   bdo:PersonOtherName ;
        rdfs:label          "Ārya-Vimuktiṣena"@sa-x-iast .
    
    bdr:NT72935E98221E1698
        a                   bdo:Note ;
        bdo:noteText        "Indian Panditas"@en .
    
    bdr:NTP4CZ15637_ATII0
        a                   bdo:Note ;
        bdo:noteText        "Information from this record was contributed by the Authors and Translators Identification Initiative (ATII) project in collaboration with the Khyentse Center at Universität Hamburg."@en .
    
    bdr:NTP4CZ15637_ATII1
        a                   bdo:Note ;
        bdo:noteText        "(From the ATII project) - Author of D3788 (Abhisamayālaṃkāravṛtti). / - Dates based on Makranstky (1997: 187) and Taniguchi (2002: 25–26), who rely on Tāranātha's account of the author's association with Dignāga and Bhāviveka, and on Bu ston's gSan yig, respectively.  / - Traditional and modern authors distinguish this person from the author of D3788, who is referred to as Bhadanta-Vimuktisena."@en .
    
    bdr:P4CZ15637  a        bdo:Person ;
        bdo:note            bdr:NT72935E98221E1698 , bdr:NTP4CZ15637_ATII0 , bdr:NTP4CZ15637_ATII1 ;
        bdo:personEvent     bdr:EV95214FD912B953B5 , bdr:EVP4CZ15637_ATII_0 ;
        bdo:personGender    bdr:GenderMale ;
        bdo:personName      bdr:NM7A553D2378F8CECB , bdr:NM9FD1E5359B9F22C4 , bdr:NMB733071793762DB7 , bdr:NMC34FB8F4B772C1A4 , bdr:NMP4CZ15637_ATII_SA0 , bdr:NMP4CZ15637_ATII_SA1 ;
        bdo:personStudentOf  bdr:P6119 ;
        skos:prefLabel      "'phags pa rnam grol sde/"@bo-x-ewts , "Ārya-Vimuktisena"@sa-x-iast , "解脱军｜圣解脱军"@zh-hans .
}
