@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix edtf:  <http://id.loc.gov/datatypes/edtf/> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:P7374 {
    bda:LG0BDSVQWLDY5FLJ7S
        a                   adm:UpdateData ;
        adm:logAgent        "IATTC/scripts/write_rdf.py" ;
        adm:logDate         "2024-11-08T09:17:19.535960"^^xsd:dateTime ;
        adm:logMessage      "import data from the ATII project"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG69908D18A37FFA11
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:P7374  a            adm:AdminData ;
        adm:adminAbout      bdr:P7374 ;
        adm:facetIndex      5 ;
        adm:gitPath         "08/P7374.trig" ;
        adm:gitRepo         bda:GR0006 ;
        adm:graphId         bdg:P7374 ;
        adm:logEntry        bda:LG0BDSVQWLDY5FLJ7S , bda:LG69908D18A37FFA11 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVP7374_ATII_0
        a                   bdo:PersonBirth ;
        bdo:eventWhen       "0590"^^edtf:EDTF .
    
    bdr:EVP7374_ATII_1
        a                   bdo:PersonDeath ;
        bdo:eventWhen       "0647"^^edtf:EDTF .
    
    bdr:NM784EC26206E04E78
        a                   bdo:PersonPersonalName ;
        rdfs:label          "dga' ba'i lha/"@bo-x-ewts .
    
    bdr:NMP7374_ATII_BO0
        a                   bdo:PersonOtherName ;
        rdfs:label          "kha che'i rgyal po shrI ha ri sha de ba/"@bo-x-ewts .
    
    bdr:NMP7374_ATII_BO1
        a                   bdo:PersonOtherName ;
        rdfs:label          "dpal dga' ba'i lha/"@bo-x-ewts .
    
    bdr:NMP7374_ATII_BO2
        a                   bdo:PersonOtherName ;
        rdfs:label          "ha ri sha de ba/"@bo-x-ewts .
    
    bdr:NMP7374_ATII_SA0
        a                   bdo:PersonOtherName ;
        rdfs:label          "Harṣadeva"@sa-x-iast .
    
    bdr:NMP7374_ATII_SA1
        a                   bdo:PersonOtherName ;
        rdfs:label          "Harṣavardhana"@sa-x-iast .
    
    bdr:NTP7374_ATII0
        a                   bdo:Note ;
        bdo:noteText        "Information from this record was contributed by the Authors and Translators Identification Initiative (ATII) project in collaboration with the Khyentse Center at Universität Hamburg."@en .
    
    bdr:NTP7374_ATII1
        a                   bdo:Note ;
        bdo:noteText        "(From the ATII project) - Dates based on Franceschini 2019. / - His name is occasionally written, in Sanskrit and in Tibetan, along with the prefix Śrī; but this figure is not to be confused with the 12th century poet Śrīharṣa. / - That D1117 (chos kyi dbyings su bstod pa), attributed to Rab dga' lha, is by Harṣa is confirmed by Hahn 1996."@en .
    
    bdr:P7374  a            bdo:Person ;
        bdo:note            bdr:NTP7374_ATII0 , bdr:NTP7374_ATII1 ;
        bdo:personEvent     bdr:EVP7374_ATII_0 , bdr:EVP7374_ATII_1 ;
        bdo:personGender    bdr:GenderMale ;
        bdo:personName      bdr:NM784EC26206E04E78 , bdr:NMP7374_ATII_BO0 , bdr:NMP7374_ATII_BO1 , bdr:NMP7374_ATII_BO2 , bdr:NMP7374_ATII_SA0 , bdr:NMP7374_ATII_SA1 ;
        skos:prefLabel      "dga' ba'i lha/"@bo-x-ewts , "Harṣa"@sa-x-iast .
}
