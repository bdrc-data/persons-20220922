@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:P1TD66 {
    bda:LG0P1TD66_R5HYCRVLE1DV
        a                   adm:InitialDataCreation ;
        adm:logDate         "2024-01-26T02:38:42.068730Z"^^xsd:dateTime ;
        adm:logMessage      "Created record, added event, added bio notes"@en ;
        adm:logWho          bdu:U809979284 .
    
    bda:P1TD66  a           adm:AdminData ;
        adm:adminAbout      bdr:P1TD66 ;
        adm:graphId         bdg:P1TD66 ;
        adm:logEntry        bda:LG0P1TD66_R5HYCRVLE1DV ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVP1TD6652B6JUAI
        a                   bdo:PersonBirth ;
        bdo:eventWhen       "1981"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWhere      bdr:G3188 .
    
    bdr:NMP1TD6697T0ZOS4
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "ཤར་གླིང་ཟླ་སྒྲོན།"@bo .
    
    bdr:NMP1TD66H8NU0EGU
        a                   bdo:PersonPrimaryTitle ;
        rdfs:label          "ཟླ་སྒྲོན་ཤར་གླིང་།"@bo .
    
    bdr:NMP1TD66TEBP75L1
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "ཟླ་སྒྲོན་ཤར་གླིང་།"@bo .
    
    bdr:NMP1TD66UTOIEA2V
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "Dhardon Sharling"@en .
    
    bdr:NTP1TD66JRKA8GKA
        a                   bdo:Note ;
        bdo:noteText        "ཟླ་སྒྲོན་ཤར་གླིང་ནི་བོད་ཀྱི་བུད་མེད་དབུ་ཁྲིད་ཅིག་དང་མཁས་པ་ཞིག་ཡིན། ༡༩༨༡ ལོར་རྒྱ་གར་དུ་སྐྱེས་ཤིང་། རྒྱ་གར་བྱང་ཕྱོགས་བོད་ཕྲུག་ཁྱིམ་སྡེའི་སློབ་གྲྭར་སློབ་སྦྱོང་བྱས་ཡོད། མོས་དྷརྨ་ས་ལར་བོད་ཀྱི་བུད་མེད་ཚོགས་པ་ནས་འགོ་བཙུགས་ཏེ་བོད་པའི་སྤྱི་ཚོགས་དང་ཁྲིམས་རྩོད་པའི་ལས་དོན་ལ་ལོ་མང་རིང་ལས་ཀ་བྱས་ཡོད། ཁོང་ནི་ཕྱི་ལོ ༢༠༡༡ ནས ༢༠༡༦ བར་བོད་གཟུང་སྤྱི་འཐུས་ཀྱི་འཐུས་མི་ཡིན་པ་དང་། ཕྱི་ལོ ༢༠༡༦ ནས ༢༠༡༩ བར་བོད་ཞུང་དབུས་འཛིན་སྐྱོང་ལྷན་ཁང་གི་བརྡ་འཕྲིན་དང་རྒྱལ་སྤྱིའི་འབྲེལ་ལམ་སྡེ་ཚན་གྱི་དྲུང་ཡིག་དང་། བོད་ཀྱི་འགྲེམས་སྟོན་ཁང་གསར་པའི་་ལས་འཆར་འགོ་ཁྲིད་ཡིན། དེ་བཞིན་བོད་དབུས་གཞུང་གིས་བོད་པའི་བུད་མེད་ལ་དབང་ཆའི་ཚོགས་འདུ་དང་པོ་དེ་འཚོགས་པར་རམ་འདེགས་བྱས་ཡོད། ཟླ་སྒྲོན་ཤར་གླིང་ནི་ཕྱི་ལོ ༢༠༢༣ ལོར་ཨ་རིའི་མངའ་སྡེ་མཱ་སེ་ཆུའུ་སེཊ་ཨམ་ཧར་སི་ཊི་མཐོ་སློབ་ནས་བརྡ་འཕྲིན་་རིག་པའི་སློབ་དཔོན་གྱི་གོ་གནས་ཐོབ།"@bo .
    
    bdr:NTP1TD66N1FW54YF
        a                   bdo:Note ;
        bdo:noteText        "Dhardon Sharling is a Tibetan woman leader and scholar.\nBorn in India in 1981, she was educated at the Tibetan Children's Village schools in northern India. She worked in Tibetan civil society and advocacy for many years, starting with the Tibetan Women's Association in Dharamsala. She was a Tibetan Parliamentarian from 2011-2016, and served as Secretary of the Department of Information and International Relations of the Central Tibetan Administration from 2016-2019. She was project leader for the New Tibet Museum and helped convene the first Tibetan Women's Empowerment Conference by the CTA. Dhardon Sharling received her PhD in Communications from the University of Massachusetts, Amherst, in 2023.\n"@en .
    
    bdr:P1TD66  a           bdo:Person ;
        skos:prefLabel      "ཟླ་སྒྲོན་ཤར་གླིང་།"@bo ;
        bdo:note            bdr:NTP1TD66JRKA8GKA , bdr:NTP1TD66N1FW54YF ;
        bdo:personEvent     bdr:EVP1TD6652B6JUAI ;
        bdo:personGender    bdr:GenderFemale ;
        bdo:personName      bdr:NMP1TD6697T0ZOS4 , bdr:NMP1TD66H8NU0EGU , bdr:NMP1TD66TEBP75L1 , bdr:NMP1TD66UTOIEA2V .
}
