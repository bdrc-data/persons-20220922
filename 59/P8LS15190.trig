@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix edtf:  <http://id.loc.gov/datatypes/edtf/> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:P8LS15190 {
    bda:LG0BDSVQWLDY5FLJ7S
        a                   adm:UpdateData ;
        adm:logAgent        "IATTC/scripts/write_rdf.py" ;
        adm:logDate         "2024-11-08T09:17:19.535960"^^xsd:dateTime ;
        adm:logMessage      "import data from the ATII project"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGDF075C73DEF4FAB6
        a                   adm:InitialDataCreation ;
        adm:logDate         "2014-02-04T12:28:11.619000+00:00"^^xsd:dateTime ;
        adm:logMessage      "created new person"@en ;
        adm:logWho          bdu:U00019 .
    
    bda:P8LS15190  a        adm:AdminData ;
        adm:adminAbout      bdr:P8LS15190 ;
        adm:facetIndex      5 ;
        adm:gitPath         "59/P8LS15190.trig" ;
        adm:gitRepo         bda:GR0006 ;
        adm:graphId         bdg:P8LS15190 ;
        adm:logEntry        bda:LG0BDSVQWLDY5FLJ7S , bda:LGDF075C73DEF4FAB6 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVP8LS15190_ATII_0
        a                   bdo:PersonFlourished ;
        bdo:eventWhen       "1000/1150"^^edtf:EDTF .
    
    bdr:NMF36A1FBADFED7741
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "dza ya shrI/"@bo-x-ewts .
    
    bdr:NMP8LS15190_ATII_BO0
        a                   bdo:PersonOtherName ;
        rdfs:label          "rgya gar kyi mkhan po dzA ya shrI/"@bo-x-ewts .
    
    bdr:NTD3E9162C3C91F1E7
        a                   bdo:Note ;
        bdo:contentLocationStatement  "vol. e, f. 50a1" ;
        bdo:noteSource      bdr:MW29706 .
    
    bdr:NTP8LS15190_ATII0
        a                   bdo:Note ;
        bdo:noteText        "Information from this record was contributed by the Authors and Translators Identification Initiative (ATII) project in collaboration with the Khyentse Center at Universität Hamburg."@en .
    
    bdr:NTP8LS15190_ATII1
        a                   bdo:Note ;
        bdo:noteText        "(From the ATII project) - Anālayo 2012: 323–324 n. 58: “[…] In the case of Śamathadeva's work, in the absence of any precise information Skilling 2005: 699 suggests the eleventh century to be a possible date for the translation, the work itself having been compiled \"at any time between the 5th century and the as yet unknown date of its Tibetan translation\" cf. Mejor 1991: 64, who explains that \"it seems probable that the Indian translator, Jayaśrī,\" of Śamathadeva's work \"is the same as the Kashmirian logician Jayaśrī who lived in the second half of the eleventh century\".” / - Skilling 2020: 713: “We do not know when or where Śamathadeva worked, we do not know anything about Jayaśrī, or about the Khams pa translator. We do not know the dates of any of the three figures, but it is likely that the translation was done in the eleventh or twelfth centuries, during which Kashmir was a centre for the study of Pramāṇa and Tantra and philosophy and aesthetics, and was an active centre of translation from Sanskrit into Tibetan.”"@en .
    
    bdr:P8LS15190  a        bdo:Person ;
        bdo:note            bdr:NTD3E9162C3C91F1E7 , bdr:NTP8LS15190_ATII0 , bdr:NTP8LS15190_ATII1 ;
        bdo:personEvent     bdr:EVP8LS15190_ATII_0 ;
        bdo:personGender    bdr:GenderMale ;
        bdo:personName      bdr:NMF36A1FBADFED7741 , bdr:NMP8LS15190_ATII_BO0 ;
        skos:prefLabel      "dza ya shrI/"@bo-x-ewts , "Jayaśrī"@sa-x-iast .
}
