@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:P9029 {
    bda:LG0P9029_ENMDMAQXV9KD
        a                   adm:UpdateData ;
        adm:logDate         "2024-02-04T17:07:03.329030Z"^^xsd:dateTime ;
        adm:logMessage      "added names and birthplace"@en ;
        adm:logWho          bdu:U809979284 .
    
    bda:LG0P9029_GBND4U0JCJIS
        a                   adm:UpdateData ;
        adm:logDate         "2024-01-26T13:21:50.198382Z"^^xsd:dateTime ;
        adm:logMessage      "added bio note"@bo ;
        adm:logWho          bdu:U809979284 .
    
    bda:LG854752DFA5173ABE
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:P9029  a            adm:AdminData ;
        adm:adminAbout      bdr:P9029 ;
        adm:facetIndex      5 ;
        adm:gitPath         "91/P9029.trig" ;
        adm:gitRepo         bda:GR0006 ;
        adm:graphId         bdg:P9029 ;
        adm:logEntry        bda:LG0P9029_ENMDMAQXV9KD , bda:LG0P9029_GBND4U0JCJIS , bda:LG854752DFA5173ABE ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:seeOtherToL     "https://treasuryoflives.org/biographies/view/Yuthok-Dorje-Yudon/11798"^^xsd:anyURI ;
        adm:status          bda:StatusReleased .
    
    bdr:EV75BDA066D6192244
        a                   bdo:PersonBirth ;
        bdo:eventWhen       "1912"^^<http://id.loc.gov/datatypes/edtf/EDTF> ;
        bdo:eventWhere      bdr:G2800 .
    
    bdr:EVP9029N4THWA7M
        a                   bdo:PersonDeath ;
        bdo:eventWhen       "1998"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:NM01FB3B629A599926
        a                   bdo:PersonPersonalName ;
        rdfs:label          "rdo rje g.yu sgron/"@bo-x-ewts .
    
    bdr:NM1FF03D8ED2F2626F
        a                   bdo:PersonPersonalName ;
        rdfs:label          "g.yu thog lha lcam rdo rje g.yu sgron/"@bo-x-ewts .
    
    bdr:NMP9029HZLADNYH
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "g.yu thog rdo rje g.yu sgron/"@en .
    
    bdr:NMP9029N1YCE9F6
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "Dorje Yudon Yuthok"@en .
    
    bdr:NMP9029W7FHGOM2
        a                   bdo:PersonPersonalName ;
        rdfs:label          "Dorje Yudon"@en .
    
    bdr:NMP9029XFNLH6B5
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "Yuthok Dorje Yudon"@en .
    
    bdr:NTP9029IXM6PGB0
        a                   bdo:Note ;
        bdo:noteText        "Yuthok Dorje Yudon was a Tibetan aristocrat and a writer. Born in 1912 to the Surkhang family in Lhasa. She attended school in Lhasa. She married Yuthok Tashi Dhondup (གཡུ་ཐོག་བཀྲ་ཤིས་དོན་གྲུབ༏), a general in the Tibetan army, but later they separated. After the Communist takeover of Tibet in 1959, she went into exile in India along with her family. In the late 1960s, she moved to the United states. She wrote an autobiography of her life in English titled House of the Turquoise Roof, which was published in 1990. She died in 1998. "@en .
    
    bdr:NTP9029VDOQ6FH0
        a                   bdo:Note ;
        bdo:noteText        "གཡུ་ཐོག་རྡོ་རྗེ་གཡུ་སྒྲོན་ནི་སྐུ་དྲག་དང་རྩོམ་པ་པོ་ཞིག་ཡིན། ཕྱི་ལོ ༡༩༡༢ ལོར་ལྷ་སའི་ཟུར་ཁང་ཁྱིམ་དུ་སྐྱེས། དེ་ནས་ལྷ་སར་སློབ་གྲྭར་བསྐྱོད་དེ་བོད་དམག་གི་དམག་དཔོན་གཡུ་ཐོག་བཀྲ་ཤིས་དོན་གྲུབ་དང་གཉེན་སྒྲིག་བྱས་ཀྱང་རྗེས་སུ་ཁ་གྱེས། ༡༩༥༩ ལོར་བོད་རྒྱ་མིས་བཙན་བཟུང་བྱས་རྗེས་ནང་མི་རྣམས་དང་མཉམ་དུ་རྒྱ་གར་དུ་བཙན་བྱོལ་དུ་སླེབས། ༡༩༦༠ གོ་གྲངས་སྨད་དུ་ཨ་རིར་གནས་སྤོ་བྱས་ནས་རང་གི་མི་ཚེའི་ལོ་རྒྱུས་དབྱིན་ཡིག་ཏུ་རང་རྣམ་གཡུ་ཐོག་ཁང་ཞེས་པ་བརྩམས་ཏེ་ཕྱི་ལོ ༡༩༩༠ ལོར་པར་སྐྲུན་བྱས། ཕྱི་ལོ ༡༩༩༨ ལོར་གྲོངས།།"@bo .
    
    bdr:P9029  a            bdo:Person ;
        skos:prefLabel      "g.yu thog lha lcam rdo rje g.yu sgron/"@bo-x-ewts , "Yuthok Dorje Yudon"@en ;
        bdo:associatedTradition  bdr:TraditionGeluk ;
        bdo:hasBrother      bdr:P9031 ;
        bdo:hasFather       bdr:P9021 ;
        bdo:hasMother       bdr:P9027 ;
        bdo:note            bdr:NTP9029IXM6PGB0 , bdr:NTP9029VDOQ6FH0 ;
        bdo:personEvent     bdr:EV75BDA066D6192244 , bdr:EVP9029N4THWA7M ;
        bdo:personGender    bdr:GenderFemale ;
        bdo:personName      bdr:NM01FB3B629A599926 , bdr:NM1FF03D8ED2F2626F , bdr:NMP9029HZLADNYH , bdr:NMP9029N1YCE9F6 , bdr:NMP9029W7FHGOM2 , bdr:NMP9029XFNLH6B5 .
}
