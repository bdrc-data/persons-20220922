@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix edtf:  <http://id.loc.gov/datatypes/edtf/> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:P8LS12196 {
    bda:LG0BDSPNC8IPF9SUO0
        a                   adm:UpdateData ;
        adm:logAgent        "tol-import/import-dates.py" ;
        adm:logDate         "2024-12-10T09:41:42.577659"^^xsd:dateTime ;
        adm:logMessage      "import dates from ToL"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG540290201EFD8AE5
        a                   adm:InitialDataCreation ;
        adm:logDate         "2012-08-06T11:08:42.557000+00:00"^^xsd:dateTime ;
        adm:logMessage      "created new person record"@en ;
        adm:logWho          bdu:U00019 .
    
    bda:LG643A70F778CB6977
        a                   adm:UpdateData ;
        adm:logDate         "2012-09-21T14:14:43.700000+00:00"^^xsd:dateTime ;
        adm:logMessage      "corrected incarnation relation"@en ;
        adm:logWho          bdu:U00020 .
    
    bda:LG7021165509AD1450
        a                   adm:UpdateData ;
        adm:logDate         "2014-09-22T10:38:48.273000+00:00"^^xsd:dateTime ;
        adm:logMessage      "teacher student"@en ;
        adm:logWho          bdu:U00002 .
    
    bda:LGB57A2E993C8BF1B8
        a                   adm:UpdateData ;
        adm:logDate         "2013-02-04T09:17:07.863000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added tulku title and incarnation relation"@en ;
        adm:logWho          bdu:U00019 .
    
    bda:LGIM664D0289A7
        a                   adm:UpdateData ;
        adm:logDate         "2014-03-20T14:27:21.266000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added tbrc phonetic tulku title"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:P8LS12196  a        adm:AdminData ;
        adm:adminAbout      bdr:P8LS12196 ;
        adm:facetIndex      17 ;
        adm:gitPath         "f4/P8LS12196.trig" ;
        adm:gitRepo         bda:GR0006 ;
        adm:graphId         bdg:P8LS12196 ;
        adm:logEntry        bda:LG0BDSPNC8IPF9SUO0 , bda:LG540290201EFD8AE5 , bda:LG643A70F778CB6977 , bda:LG7021165509AD1450 , bda:LGB57A2E993C8BF1B8 , bda:LGIM664D0289A7 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:seeOtherToL     "https://treasuryoflives.org/biographies/view/Demchok-Dorje/13204"^^xsd:anyURI ;
        adm:status          bda:StatusReleased .
    
    bdr:EV9533F0E395A847B3
        a                   bdo:PersonOccupiesSeat ;
        bdo:eventWhere      bdr:G16 .
    
    bdr:EVD102A96E895DDA74
        a                   bdo:PersonAssumesOffice ;
        bdo:eventWhere      bdr:G16 ;
        bdo:role            bdr:R8LS12188 .
    
    bdr:EVP8LS12196_ATII_0
        a                   bdo:PersonBirth ;
        bdo:eventWhen       "1898"^^edtf:EDTF .
    
    bdr:EVP8LS12196_ATII_1
        a                   bdo:PersonDeath ;
        bdo:eventWhen       "1940"^^edtf:EDTF .
    
    bdr:NM1E34345E672C0959
        a                   bdo:PersonPrimaryName ;
        rdfs:label          "bde mchog rdo rje/"@bo-x-ewts .
    
    bdr:NM3F83E212E4CDF2F9
        a                   bdo:PersonOfficeTitle ;
        rdfs:label          "rdzogs chen gdan rabs 10"@bo-x-ewts .
    
    bdr:NM848D5D8100A4D577
        a                   bdo:PersonPrimaryTitle ;
        rdfs:label          "mkhan sprul bde mchog rdo rje/"@bo-x-ewts .
    
    bdr:NMAC9396DAFC178667
        a                   bdo:PersonTulkuTitle ;
        rdfs:label          "pad+ma ba dz+ra 02"@bo-x-ewts .
    
    bdr:NMCD768907A4E95B34
        a                   bdo:PersonTulkuTitle ;
        rdfs:label          "Pema Vajra 02"@bo-x-phon-en-m-tbrc .
    
    bdr:NT2A08A1A800D7062E
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 383-384" ;
        bdo:noteSource      bdr:MW27401 ;
        bdo:noteText        "See for sku rabs reference"@en .
    
    bdr:NT8B1004D007937464
        a                   bdo:Note ;
        bdo:contentLocationStatement  "p. 363-364" ;
        bdo:noteSource      bdr:MW27401 ;
        bdo:noteText        "He was recognised as an incarnate of mchog sprul bde dga' in the sku rabs of lama of pad+ma badz+ra (8th abbot of rdzogs chen shri seng) by the fifth rdzogs chen thub bstan chos kyi rdo rje."@en .
    
    bdr:NTP8LS12196_TOL0
        a                   bdo:Note ;
        bdo:noteText        "Information from this record was contributed by Treasury of Lives."@en .
    
    bdr:P8LS12196  a        bdo:Person ;
        bdo:associatedTradition  bdr:TraditionNyingma ;
        bdo:incarnationGeneral  bdr:P6744 , bdr:P6960 ;
        bdo:note            bdr:NT2A08A1A800D7062E , bdr:NT8B1004D007937464 , bdr:NTP8LS12196_TOL0 ;
        bdo:personEvent     bdr:EV9533F0E395A847B3 , bdr:EVD102A96E895DDA74 , bdr:EVP8LS12196_ATII_0 , bdr:EVP8LS12196_ATII_1 ;
        bdo:personName      bdr:NM1E34345E672C0959 , bdr:NM3F83E212E4CDF2F9 , bdr:NM848D5D8100A4D577 , bdr:NMAC9396DAFC178667 , bdr:NMCD768907A4E95B34 ;
        bdo:personStudentOf  bdr:P252 , bdr:P699 , bdr:P701 , bdr:P8702 , bdr:P8704 ;
        skos:prefLabel      "mkhan sprul bde mchog rdo rje/"@bo-x-ewts .
}
